import models.InvoiceItem;

public class InvoiceItemClass{
    public static void main(String[] args) throws Exception {
        
        InvoiceItem invoiceItem1 = new InvoiceItem("1", "Ga", 10, 1.5);
        InvoiceItem invoiceItem2 = new InvoiceItem("2", "Vit", 15, 2.5);

        System.out.println("toString của invoiceItem 1 là: " + invoiceItem1.toString());
        System.out.println("Tổng giá của đơn 1 là: " + invoiceItem1.getTotal());

        System.out.println("----------------------------------------------------------");
        System.out.println("toString của invoiceItem 2 là: " + invoiceItem2.toString());
        System.out.println("Tổng giá của đơn 2 là: " + invoiceItem2.getTotal());
    }
}
